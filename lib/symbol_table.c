/* 
 * Copyright (C) 1999, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2011,
 * 2012, 2013 Free Software Foundation, Inc.
 * 
 * This file is part of GNU libmatheval
 * 
 * GNU libmatheval is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * GNU libmatheval is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNU libmatheval.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#if HAVE_CONFIG_H
#include "config.h"
#endif

#include <assert.h>
#include <stdarg.h>
#include "common.h"
#include "symbol_table.h"
#include "xmath.h"

/* Type definition for function accepting single argument of double
 * type and returning double value. */
typedef double  (*function_type) (double);
/* Type definition for function accepting two arguments of double
 * type and returning double value. */
typedef double  (*function2_type) (double,double);
/* Type definition for function accepting three arguments of double
 * type and returning double value. */
typedef double  (*function3_type) (double,double,double);
/* Type definition for function accepting four arguments of double
 * type and returning double value. */
typedef double  (*function4_type) (double,double,double,double);

/* Calculate hash value for given name and hash table length.  */
static int      hash(char *name, int length);

SymbolTable    *
symbol_table_create(int length)
{
	SymbolTable    *symbol_table;	/* Pointer to symbol table.  */

	static char    *constants_names[] = {
		"e", "log2e", "log10e", "ln2", "ln10", "pi", "pi_2",
		"pi_4", "1_pi", "2_pi", "2_sqrtpi", "sqrt2", "sqrt1_2",
        "NaN",
		/* gsl constants */
#ifdef USE_GSL_CONST
		"C_c", "C_mu0", "C_eps0", "C_h", "C_hbar", "C_faraday",
		"C_kb", "C_R0", "C_V0", "C_sigma", "C_gauss",
#endif
	};			/* Symbol table predefined constants
				 * names. */

	static double   constants[] = {
        /* libm constants */
		M_E, M_LOG2E, M_LOG10E, M_LN2, M_LN10, M_PI, M_PI_2,
        M_PI_4, M_1_PI, M_2_PI, M_2_SQRTPI, M_SQRT2, M_SQRT1_2,
        MATH_NAN,
		/* gsl constants */
#ifdef USE_GSL_CONST
		GSL_CONST_MKSA_SPEED_OF_LIGHT, GSL_CONST_MKSA_VACUUM_PERMEABILITY,
		GSL_CONST_MKSA_VACUUM_PERMITTIVITY, GSL_CONST_MKSA_PLANCKS_CONSTANT_H,
		GSL_CONST_MKSA_PLANCKS_CONSTANT_HBAR, GSL_CONST_MKSA_FARADAY,
		GSL_CONST_MKSA_BOLTZMANN, GSL_CONST_MKSA_MOLAR_GAS,
		GSL_CONST_MKSA_STANDARD_GAS_VOLUME,
		GSL_CONST_MKSA_STEFAN_BOLTZMANN_CONSTANT, GSL_CONST_MKSA_GAUSS
#endif
	};			/* Symbol table predefined constants
				 * values. */

	static char    *functions_names[] = {
		/* libm functions */
		"acos", "acosh", "asin", "asinh", "atan", "atanh", "cbrt",
		"ceil", "cos", "cosh", "erf", "erfc", "exp", "exp2", "expm1",
		"abs", "floor", "lgamma", "log", "log10",
		"log1p", "log2", "round", "sin", "sinh", "sqrt",
		"tan", "tanh", "gamma", "trunc",
#ifdef _XOPEN_SOURCE
        "J0", "J1", "Y0", "Y1",
#endif
		/* additional functions */
		"cot", "sec", "csc", "acot", "asec", "acsc", "coth", "sech",
		"csch", "acoth", "asech", "acsch", "step", "delta", "nandelta",
		"sign",
		/* gsl functions */
#ifdef USE_GSL
		/* trigonometric functions */
		"sinc", "lsinh", "lcosh",
		/* exponential integrals */
		"E1", "E2", "Ei", "Shi", "Chi", "Si", "Ci", "atanint", "li", "Li",
		"Ei3",
		/* Airy functions */
		"Ai", "Bi", "Ais", "Bis", "Aip", "Bip", "Aips", "Bips",
		/* digamma, trigamma and dilogarithm */
		"digamma", "digamma1piy", "trigamma", "dilog",
		"gammastar", "gammainv",
		/* zeta functions */
		"zeta", "zetam1", "eta",
		/* Lambert W functions */
		"W0", "Wm1",
		/* Clausen, Dawson and Debye functions */
		"clausen", "dawson", "debye1", "debye2", "debye3", "debye4",
		"debye5", "debye6",
		/* complete Fermi-Dirac integrals */
		"fdm1", "fd0", "fd1", "fd2", "fdmhalf", "fdhalf", "fd3half",
		/* error functions */
		"lerfc", "erfZ", "erfQ", "hazard",
		/* logarithmic functions */
		"labs", "log1px", "log1pxmx",
		/* exponential functions */
		"expm1", "exprel", "exprel2",
		/* synchrotron and transport functions */
		"synch1", "synch2", "transp2", "transp3", "transp4", "transp5",
		/* elliptic integrals */
		"ellintKc", "ellintEc",
		/* legendre polynomials and functions */
		"legendreP1", "legendreP2", "legendreP3", "legendreQ0",
		"legendreQ1",
		/* Bessel functions */
		"besselI0", "besselI1", "besselI0s", "besselI1s", "besselK0",
		"besselK1", "besselK0s", "besselK1s", "besselj0", "besselj1",
		"besselj2", "bessely0", "bessely1", "bessely2",
#endif
	};			/* Symbol table predefined functions
				 * names. */

	static double   (*functions[]) (double) = {
		/* libm functions */
		acos, acosh, asin, asinh, atan, atanh, cbrt, ceil, cos, cosh,
		erf, erfc, exp, exp2, expm1, fabs, floor, lgamma, log,
		log10, log1p, log2, round, sin, sinh, sqrt, tan, tanh,
		tgamma, trunc,
#ifdef _XOPEN_SOURCE
        j0, j1, y0, y1,
#endif
		/* additional functions */
		math_cot, math_sec, math_csc, math_acot, math_asec, math_acsc,
		math_coth, math_sech, math_csch, math_acoth, math_asech, math_acsch,
		math_step, math_delta, math_nandelta, math_sign,
		/* gsl functions */
#ifdef USE_GSL
		/* trigonometric functions */
		math_sinc, math_lsinh, math_lcosh,
		/* exponential integrals */
		math_E1, math_E2, math_Ei, math_Shi, math_Chi, math_Si, math_Ci,
		math_atanint, math_li, math_Li, math_Ei3,
		/* Airy functions */
		math_Ai, math_Bi, math_Ais, math_Bis, math_Aip, math_Bip,
		math_Aips, math_Bips,
		/* digamma, trigamma and dilogarithm */
		math_digamma, math_digamma1piy, math_trigamma, math_dilog,
		math_gammastar, math_gammainv,
		/* zeta functions */
		math_zeta, math_zetam1, math_eta,
		/* Lambert W functions */
		math_W0, math_Wm1,
		/* Clausen, Dawson and Debye functions */
		math_clausen, math_dawson, math_debye1, math_debye2, math_debye3,
		math_debye4, math_debye5, math_debye6,
		/* complete Fermi-Dirac integrals */
		math_fdm1, math_fd0, math_fd1, math_fd2, math_fdmhalf,
		math_fdhalf, math_fd3half,
		/* error functions */
		math_lerfc, math_erfZ, math_erfQ, math_hazard,
		/* logarithmic functions */
		math_labs, math_log1px, math_log1pxmx,
		/* exponential functions */
		math_expm1, math_exprel, math_exprel2,
		/* synchrotron and transport functions */
		math_synch1, math_synch2, math_transp2, math_transp3, math_transp4,
		math_transp4,
		/* elliptic integrals */
		math_ellintKc, math_ellintEc,
		/* legendre polynomials and functions */
		math_legendreP1, math_legendreP2, math_legendreP3,
		math_legendreQ0, math_legendreQ1,
		/* Bessel functions */
		math_besselI0, math_besselI1, math_besselI0s, math_besselI1s,
		math_besselK0, math_besselK1, math_besselK0s, math_besselK1s,
		math_besselj0, math_besselj1, math_besselj2, math_bessely0,
		math_bessely1, math_bessely2,
#endif
	};	/* Symbol table predefined functions pointers to 
		 * functions to calculate them. */

	/* Symbol table predefined names of functions of 
	 * two arguments*/
	static char    *functions2_names[] = {
		/* libm functions */
		"mod", "dist", "Jn", "Yn",
#ifdef USE_GSL
		"hzeta", "fdinc0", "beta", "lbeta",	"gammainc",
		"gammaincQ", "gammaincP", "En", "poch", "lpoch", "pochrel",
		"polygamma", "hyperg0F1", "expmul", "expreln", "gegenb1",
		"gegenb2", "gegenb3", "hydrogenR1", "coulombnorm",
		"ellipsn", "ellipcn", "ellipdn", "ellintPc", "ellintF",
		"ellintE", "ellintRC", "laguerre1", "laguerre2", "laguerre3",
		"legendrePl", "legendreQl", "conicPhalf", "conicPmhalf",
		"conicP0", "conicP1", "legendreH3d0", "legendreH3d1",
		"mathieua", "mathieub", "besselIn", "besselIns", "besselKn",
		"besselKns", "besseljl", "besselyl", "besselJnu", "besselYnu",
		"besselInu", "besselKnu", "ellintD",
#endif
	};

	/* Symbol table predefined function pointers to functions
	 * of two arguments */
	static double   (*functions2[]) (double,double) = {
		fmod, hypot, math_jn, math_yn,
#ifdef USE_GSL
		math2_hzeta, math2_fdinc0, math2_beta, math2_lbeta,
		math2_gammainc, math2_gammaincQ, math2_gammaincP, math2_En,
		math2_poch, math2_lpoch, math2_pochrel, math2_polygamma,
		math2_hyperg0F1, math2_expmul, math2_expreln, math2_gegenb1,
		math2_gegenb2, math2_gegenb3, math2_hydrogenR1, math2_coulombnorm,
		math2_ellipsn, math2_ellipcn, math2_ellipdn, math2_ellintPc,
		math2_ellintF, math2_ellintE, math2_ellintRC, math2_laguerre1,
		math2_laguerre2, math2_laguerre3, math2_legendrePl,
		math2_legendreQl, math2_conicPhalf, math2_conicPmhalf,
		math2_conicP0, math2_conicP1, math2_legendreH3d0,
		math2_legendreH3d1, math2_mathieua, math2_mathieub,
		math2_besselIn, math2_besselIns, math2_besselKn, math2_besselKns,
		math2_besseljl, math2_besselyl, math2_besselJnu, math2_besselYnu,
		math2_besselInu, math2_besselKnu, math2_ellintD,
#endif
	};

	/* Symbol table predefined names of functions of 
	 * three arguments*/
	static char    *functions3_names[] = {
#ifdef USE_GSL
		"betainc", "hyperg1F1", "hypergU", "hyperg2F0", "gegenbn",
		"ellintP", "ellintRD", "ellintRF", "laguerren",
		"legendrePlm", "sphPlm", "conicPsph", "conicPcyl",
		"legendreH3d", "mathieuce", "mathieuse",
#endif
	};

	/* Symbol table predefined function pointers to functions
	 * of three arguments */
	static double   (*functions3[]) (double,double,double) = {
#ifdef USE_GSL
		math3_betainc, math3_hyperg1F1, math3_hypergU, math3_hyperg2F0,
		math3_gegenbn, math3_ellintP, math3_ellintRD,
		math3_ellintRF, math3_laguerren, math3_legendrePlm, math3_sphPlm,
		math3_conicPsph, math3_conicPcyl, math3_legendreH3d,
		math3_mathieuce, math3_mathieuse,
#endif
	};

	/* Symbol table predefined names of functions of 
	 * four arguments*/
	static char    *functions4_names[] = {
#ifdef USE_GSL
		"hyperg2F1", "hyperg2F1c", "hyperg2F1r", "hyperg2F1cr",
		"hydrogenR", "coulombF", "coulombFp", "coulombG", "coulombGp",
		"ellintRJ", "mathieuMc", "mathieuMs",
#endif
	};

	/* Symbol table predefined function pointers to functions
	 * of four arguments */
	static double   (*functions4[]) (double,double,double,double) = {
#ifdef USE_GSL
		math4_hyperg2F1, math4_hyperg2F1c, math4_hyperg2F1r,
		math4_hyperg2F1cr, math4_hydrogenR, math4_coulombF, math4_coulombFp,
		math4_coulombG, math4_coulombGp, math4_ellintRJ,
		math4_mathieuMc, math4_mathieuMs,
#endif
	};

	long unsigned int    i;	/* Loop counter.  */

	/* Allocate memory for symbol table data structure as well as for
	 * corresponding hash table. */
	symbol_table = XMALLOC(SymbolTable, 1);
	symbol_table->length = length;
	symbol_table->records = XCALLOC(Record, symbol_table->length);

	/* Insert predefined constants into symbol table. */
	for (i = 0;
	     i < sizeof(constants_names) / sizeof(constants_names[0]); i++)
		symbol_table_insert(symbol_table, constants_names[i], 'c',
				    constants[i]);

	/* Insert predefined functions into symbol table. */
	for (i = 0;
	     i < sizeof(functions_names) / sizeof(functions_names[0]); i++)
		symbol_table_insert(symbol_table, functions_names[i], 'f',
				    functions[i]);

	/* Insert predefined functions of two arguments into symbol table. */
	for (i = 0;
	     i < sizeof(functions2_names) / sizeof(functions2_names[0]); i++)
		symbol_table_insert(symbol_table, functions2_names[i], 'g',
				    functions2[i]);

	/* Insert predefined functions of three arguments into symbol table. */
	for (i = 0;
	     i < sizeof(functions3_names) / sizeof(functions3_names[0]); i++)
		symbol_table_insert(symbol_table, functions3_names[i], 'h',
				    functions3[i]);

	/* Insert predefined functions of four arguments into symbol table. */
	for (i = 0;
	     i < sizeof(functions4_names) / sizeof(functions4_names[0]); i++)
		symbol_table_insert(symbol_table, functions4_names[i], 'q',
				    functions4[i]);

	/* Initialize symbol table reference count. */
	symbol_table->reference_count = 1;

	return symbol_table;
}

void
symbol_table_destroy(SymbolTable * symbol_table)
{
	Record         *curr,
	               *next;	/* Pointers to current and next record
				 * while traversing hash table bucket.  */
	int             i;	/* Loop counter.  */

	/* Decrement refernce count and return if symbol table still used
	 * elsewhere. */
	if (--symbol_table->reference_count > 0)
		return;

	/* Delete hash table as well as data structure representing symbol 
	 * table. */
	for (i = 0; i < symbol_table->length; i++)
		for (curr = symbol_table->records[i].next; curr;) {
			next = curr->next;
			XFREE(curr->name);
			XFREE(curr);
			curr = next;
		}
	XFREE(symbol_table->records);
	XFREE(symbol_table);
}

Record         *
symbol_table_insert(SymbolTable * symbol_table, char *name, char type, ...)
{
	Record         *record;	/* Pointer to symbol table record
				 * corresponding to name given.  */
	va_list         ap;	/* Function variable argument list.  */
	int             i;	/* Loop counter.  */

	/* Check if symbol already in table and, if affirmative and record 
	 * type same as type given, return corresponding record
	 * immediately. */
	if ((record = symbol_table_lookup(symbol_table, name))) {
		assert(record->type == type);
		return record;
	}

	/* Allocate memory for and initialize new record. */
	record = XMALLOC(Record, 1);
	record->name = XMALLOC(char, strlen(name) + 1);
	strcpy(record->name, name);
	record->type = type;
	record->flag = FALSE;

	/* Parse function variable argument list to complete record
	 * initialization. */
	va_start(ap, type);
	switch (record->type) {
	case 'c':
		record->data.value = va_arg(ap, double);
		break;

	case 'v':
		record->data.value = 0;
		break;

	case 'f':
		record->data.function = va_arg(ap, function_type);
		break;

	case 'g':
		record->data.function2 = va_arg(ap, function2_type);
		break;

	case 'h':
		record->data.function3 = va_arg(ap, function3_type);
		break;

	case 'q':
		record->data.function4 = va_arg(ap, function4_type);
		break;
	}
	va_end(ap);

	/* Calculate hash value and put record in corresponding hash table 
	 * bucket. */
	i = hash(name, symbol_table->length);
	record->next = symbol_table->records[i].next;
	symbol_table->records[i].next = record;

	return record;
}

Record         *
symbol_table_lookup(SymbolTable * symbol_table, char *name)
{
	int             i;	/* Hash value. */
	Record         *curr;	/* Pointer to current symbol table record. 
				 */

	/* 
	 * Calcuate hash value for name given.
	 */
	i = hash(name, symbol_table->length);

	/* Lookup for name in hash table bucket corresponding to above
	 * hash value. */
	for (curr = symbol_table->records[i].next; curr; curr = curr->next)
		if (!strcmp(curr->name, name))
			return curr;

	return NULL;
}

void
symbol_table_clear_flags(SymbolTable * symbol_table)
{
	Record         *curr;	/* Pointer to current symbol table record
				 * while traversing hash table bucket. */
	int             i;	/* Loop counter.  */

	/* Clear flag for all records in symbol table. */
	for (i = 0; i < symbol_table->length; i++)
		for (curr = symbol_table->records[i].next; curr;
		     curr = curr->next)
			curr->flag = FALSE;
}

int
symbol_table_get_flagged_count(SymbolTable * symbol_table)
{
	int             count;	/* Number of flagged symbol table records. 
				 */
	Record         *curr;	/* Pointer to current symbol table record
				 * while traversing hash table bucket. */
	int             i;	/* Loop counter.  */

	/* Calculate number of records in symbol table with flag set. */
	count = 0;
	for (i = 0; i < symbol_table->length; i++)
		for (curr = symbol_table->records[i].next; curr;
		     curr = curr->next)
			if (curr->flag)
				count++;
	return count;
}

int
symbol_table_get_flagged(SymbolTable * symbol_table, Record ** records,
			 int length)
{
	int             count;	/* Number of pointers to symbol table
				 * records put into given array. */
	Record         *curr;	/* Pointers to current symbol table record 
				 * while traversing hash table bucket.  */
	int             i;	/* Loop counter.  */

	/* Put pointers to records in symbol table with flag set into
	 * given array. */
	count = 0;
	for (i = 0; i < symbol_table->length; i++)
		for (curr = symbol_table->records[i].next; curr;
		     curr = curr->next)
			if (curr->flag) {
				records[count++] = curr;
				if (count == length)
					return count;
			}
	return count;
}

SymbolTable    *
symbol_table_assign(SymbolTable * symbol_table)
{
	/* Increase symbol table reference count and return pointer to
	 * data structure representing table. */
	symbol_table->reference_count++;
	return symbol_table;
}

/* Function below reused from A.V. Aho, R. Sethi, J.D. Ullman, "Compilers
 * - Principle, Techniques, and Tools", Addison-Wesley, 1986, pp 435-437,
 * and in turn from P.J. Weineberger's C compiler. */
static int
hash(char *s, int n)
{
	char           *p;
	unsigned        h,
	                g;

	h = 0;

	for (p = s; *p; p++) {
		h = (h << 4) + *p;
		if ((g = h & 0xf0000000)) {
			h = h ^ (g >> 24);
			h = h ^ g;
		}
	}

	return h % n;
}
