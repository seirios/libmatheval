/* 
 * Copyright (C) 1999, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2011,
 * 2012, 2013 Free Software Foundation, Inc.
 * 
 * This file is part of GNU libmatheval
 * 
 * GNU libmatheval is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * GNU libmatheval is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNU libmatheval.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "xmath.h"

double
math_cot(double x)
{
	/* 
	 * Calculate cotangent value.
	 */
	return 1.0 / tan(x);
}

double
math_sec(double x)
{
	/* 
	 * Calculate secant value.
	 */
	return 1.0 / cos(x);
}

double
math_csc(double x)
{
	/* 
	 * Calculate cosecant value.
	 */
	return 1.0 / sin(x);
}

double
math_acot(double x)
{
	/* 
	 * Calculate inverse cotangent value.
	 */
	return atan(1.0 / x);
}

double
math_asec(double x)
{
	/* 
	 * Calculate inverse secant value.
	 */
	return acos(1.0 / x);
}

double
math_acsc(double x)
{
	/* 
	 * Calculate inverse cosecant value.
	 */
	return asin(1.0 / x);
}

double
math_coth(double x)
{
	/* 
	 * Calculate hyperbolic cotangent value.
	 */
	return 1.0 / tanh(x);
}

double
math_sech(double x)
{
	/* 
	 * Calculate hyperbolic secant value.
	 */
	return 1.0 / cosh(x);
}

double
math_csch(double x)
{
	/* 
	 * Calculate hyperbolic cosecant value.
	 */
	return 1.0 / sinh(x);
}

double
math_acoth(double x)
{
	/* 
	 * Calculate inverse hyperbolic cotangent value.
	 */
	return atanh(1.0 / x);
}

double
math_asech(double x)
{
	/* 
	 * Calculate inverse hyperbolic secant value.
	 */
	return acosh(1.0 / x);
}

double
math_acsch(double x)
{
	/* 
	 * Calculate inverse hyperbolic cosecant value.
	 */
	return asinh(1.0 / x);
}

double
math_step(double x)
{
	/* 
	 * Calculate step function value.
	 */
	return MATH_ISNAN(x) ? x : ((x < 0.0) ? 0.0 : 1.0);
}

double
math_delta(double x)
{
	/* 
	 * Calculate delta function value.
	 */
	return MATH_ISNAN(x) ? x : ((x == 0.0) ? MATH_INFINITY : 0.0);
}

double
math_nandelta(double x)
{
	/* 
	 * Calculate modified delta function value.
	 */
	return MATH_ISNAN(x) ? x : ((x == 0.0) ? MATH_NAN : 0.0);
}

double
math_sign(double x)
{
	/*
	 * Calculate sign function value.
	 */
	return MATH_ISNAN(x) ? x : ((x < 0.0) ? -1.0 : 1.0);
}

double
math_jn(double n, double x)
{
	/*
	 * Calculate Bessel function of the first kind of order n.
	 */
	return jn((int)n,x);
}

double
math_yn(double n, double x)
{
	/*
	 * Calculate Bessel function of the first kind of order n.
	 */
	return yn((int)n,x);
}

#ifdef USE_GSL

#define GSL_WRAP(fname,gslfname) \
double \
math_ ## fname(double x) \
{ \
	gsl_sf_result res; \
	if(gsl_sf_ ## gslfname ## _e(x, &res) == GSL_SUCCESS) \
		return res.val; \
	else\
		return MATH_NAN; \
} \

#define GSL_WRAP_PREC(fname,gslfname) \
double \
math_ ## fname(double x) \
{ \
	gsl_sf_result res; \
	if(gsl_sf_ ## gslfname ## _e(x, GSL_PREC_DOUBLE, &res) == GSL_SUCCESS) \
		return res.val; \
	else\
		return MATH_NAN; \
} \

#define GSL_WRAP2(fname,gslfname) \
double \
math2_ ## fname(double x, double y) \
{ \
	gsl_sf_result res; \
	if(gsl_sf_ ## gslfname ## _e(x, y, &res) == GSL_SUCCESS) \
		return res.val; \
	else\
		return MATH_NAN; \
} \

#define GSL_WRAP2_INT(fname,gslfname) \
double \
math2_ ## fname(double x, double y) \
{ \
	gsl_sf_result res; \
	if(gsl_sf_ ## gslfname ## _e((int)x, y, &res) == GSL_SUCCESS) \
		return res.val; \
	else\
		return MATH_NAN; \
} \

#define GSL_WRAP2_PREC(fname,gslfname) \
double \
math2_ ## fname(double x, double y) \
{ \
	gsl_sf_result res; \
	if(gsl_sf_ ## gslfname ## _e(x, y, GSL_PREC_DOUBLE, &res) == GSL_SUCCESS) \
		return res.val; \
	else\
		return MATH_NAN; \
} \

#define GSL_WRAP3(fname,gslfname) \
double \
math3_ ## fname(double x, double y, double z) \
{ \
	gsl_sf_result res; \
	if(gsl_sf_ ## gslfname ## _e(x, y, z, &res) == GSL_SUCCESS) \
		return res.val; \
	else\
		return MATH_NAN; \
} \

#define GSL_WRAP3_INT(fname,gslfname) \
double \
math3_ ## fname(double x, double y, double z) \
{ \
	gsl_sf_result res; \
	if(gsl_sf_ ## gslfname ## _e((int)x, y, z, &res) == GSL_SUCCESS) \
		return res.val; \
	else\
		return MATH_NAN; \
} \

#define GSL_WRAP3_INT2(fname,gslfname) \
double \
math3_ ## fname(double x, double y, double z) \
{ \
	gsl_sf_result res; \
	if(gsl_sf_ ## gslfname ## _e((int)x, (int)y, z, &res) == GSL_SUCCESS) \
		return res.val; \
	else\
		return MATH_NAN; \
} \

#define GSL_WRAP3_PREC(fname,gslfname) \
double \
math3_ ## fname(double x, double y, double z) \
{ \
	gsl_sf_result res; \
	if(gsl_sf_ ## gslfname ## _e(x, y, z, GSL_PREC_DOUBLE, &res) == GSL_SUCCESS) \
		return res.val; \
	else\
		return MATH_NAN; \
} \

#define GSL_WRAP4(fname,gslfname) \
double \
math4_ ## fname(double x, double y, double z, double w) \
{ \
	gsl_sf_result res; \
	if(gsl_sf_ ## gslfname ## _e(x, y, z, w, &res) == GSL_SUCCESS) \
		return res.val; \
	else\
		return MATH_NAN; \
} \

#define GSL_WRAP4_PREC(fname,gslfname) \
double \
math4_ ## fname(double x, double y, double z, double w) \
{ \
	gsl_sf_result res; \
	if(gsl_sf_ ## gslfname ## _e(x, y, z, w, GSL_PREC_DOUBLE, &res) == GSL_SUCCESS) \
		return res.val; \
	else\
		return MATH_NAN; \
} \

/* Airy Functions and Derivatives */
GSL_WRAP_PREC(Ai,airy_Ai);
GSL_WRAP_PREC(Bi,airy_Bi);
GSL_WRAP_PREC(Ais,airy_Ai_scaled);
GSL_WRAP_PREC(Bis,airy_Bi_scaled);
GSL_WRAP_PREC(Aip,airy_Ai_deriv);
GSL_WRAP_PREC(Bip,airy_Bi_deriv);
GSL_WRAP_PREC(Aips,airy_Ai_deriv_scaled);
GSL_WRAP_PREC(Bips,airy_Bi_deriv_scaled);

/* Bessel Functions */
GSL_WRAP(besselI0,bessel_I0);
GSL_WRAP(besselI1,bessel_I1);
GSL_WRAP2_INT(besselIn,bessel_In);
GSL_WRAP(besselI0s,bessel_I0_scaled);
GSL_WRAP(besselI1s,bessel_I1_scaled);
GSL_WRAP2_INT(besselIns,bessel_In_scaled);
GSL_WRAP(besselK0,bessel_K0);
GSL_WRAP(besselK1,bessel_K1);
GSL_WRAP2_INT(besselKn,bessel_Kn);
GSL_WRAP(besselK0s,bessel_K0_scaled);
GSL_WRAP(besselK1s,bessel_K1_scaled);
GSL_WRAP2_INT(besselKns,bessel_Kn_scaled);
GSL_WRAP(besselj0,bessel_j0);
GSL_WRAP(besselj1,bessel_j1);
GSL_WRAP(besselj2,bessel_j2);
GSL_WRAP2_INT(besseljl,bessel_jl);
GSL_WRAP(bessely0,bessel_y0);
GSL_WRAP(bessely1,bessel_y1);
GSL_WRAP(bessely2,bessel_y2);
GSL_WRAP2_INT(besselyl,bessel_yl);
GSL_WRAP(besseli0s,bessel_i0_scaled);
GSL_WRAP(besseli1s,bessel_i1_scaled);
GSL_WRAP(besseli2s,bessel_i2_scaled);
GSL_WRAP2_INT(besselils,bessel_il_scaled);
GSL_WRAP(besselk0s,bessel_k0_scaled);
GSL_WRAP(besselk1s,bessel_k1_scaled);
GSL_WRAP(besselk2s,bessel_k2_scaled);
GSL_WRAP2_INT(besselkls,bessel_kl_scaled);
GSL_WRAP2(besselJnu,bessel_Jnu);
GSL_WRAP2(besselYnu,bessel_Ynu);
GSL_WRAP2(besselInu,bessel_Inu);
GSL_WRAP2(besselKnu,bessel_Knu);

/* Clausen Functions */
GSL_WRAP(clausen,clausen);

/* Coulomb Functions */
GSL_WRAP2(hydrogenR1,hydrogenicR_1);

double
math4_hydrogenR(double x, double y, double z, double w)
{
	gsl_sf_result res;
	if(gsl_sf_hydrogenicR_e((int)x, (int)y, z, w, &res) == GSL_SUCCESS)
		return res.val;
	else
		return MATH_NAN;
}

double
math4_coulombF(double x, double y, double z, double w)
{
	gsl_sf_result f, fp, g, gp;
	double expf, expg;
	if(gsl_sf_coulomb_wave_FG_e(x, y, z, (int)w, &f, &fp, &g, &gp, &expf, &expg) == GSL_SUCCESS)
		return f.val;
	else
		return MATH_NAN;
}

double
math4_coulombFp(double x, double y, double z, double w)
{
	gsl_sf_result f, fp, g, gp;
	double expf, expg;
	if(gsl_sf_coulomb_wave_FG_e(x, y, z, (int)w, &f, &fp, &g, &gp, &expf, &expg) == GSL_SUCCESS)
		return fp.val;
	else
		return MATH_NAN;
}

double
math4_coulombG(double x, double y, double z, double w)
{
	gsl_sf_result f, fp, g, gp;
	double expf, expg;
	if(gsl_sf_coulomb_wave_FG_e(x, y, z, (int)w, &f, &fp, &g, &gp, &expf, &expg) == GSL_SUCCESS)
		return g.val;
	else
		return MATH_NAN;
}

double
math4_coulombGp(double x, double y, double z, double w)
{
	gsl_sf_result f, fp, g, gp;
	double expf, expg;
	if(gsl_sf_coulomb_wave_FG_e(x, y, z, (int)w, &f, &fp, &g, &gp, &expf, &expg) == GSL_SUCCESS)
		return gp.val;
	else
		return MATH_NAN;
}

GSL_WRAP2(coulombnorm,coulomb_CL);

/* Dawson Function */
GSL_WRAP(dawson,dawson);

/* Debye Functions */
GSL_WRAP(debye1,debye_1);
GSL_WRAP(debye2,debye_2);
GSL_WRAP(debye3,debye_3);
GSL_WRAP(debye4,debye_4);
GSL_WRAP(debye5,debye_5);
GSL_WRAP(debye6,debye_6);

/* Dilogarithm */
GSL_WRAP(dilog,dilog);

/* Elliptic Integrals */
GSL_WRAP_PREC(ellintKc,ellint_Kcomp);
GSL_WRAP_PREC(ellintEc,ellint_Ecomp);
GSL_WRAP2_PREC(ellintPc,ellint_Pcomp);
GSL_WRAP2_PREC(ellintF,ellint_F);
GSL_WRAP2_PREC(ellintE,ellint_E);
GSL_WRAP3_PREC(ellintP,ellint_P);
GSL_WRAP2_PREC(ellintD,ellint_D);
GSL_WRAP2_PREC(ellintRC,ellint_RC);
GSL_WRAP3_PREC(ellintRD,ellint_RD);
GSL_WRAP3_PREC(ellintRF,ellint_RF);
GSL_WRAP4_PREC(ellintRJ,ellint_RJ);

/* Elliptic Functions (Jacobi) */
double
math2_ellipsn(double x, double y)
{
	double sn, cn, dn;
	if(gsl_sf_elljac_e(x, y, &sn, &cn, &dn) == GSL_SUCCESS)
		return sn;
	else
		return MATH_NAN;
}

double
math2_ellipcn(double x, double y)
{
	double sn, cn, dn;
	if(gsl_sf_elljac_e(x, y, &sn, &cn, &dn) == GSL_SUCCESS)
		return cn;
	else
		return MATH_NAN;
}

double
math2_ellipdn(double x, double y)
{
	double sn, cn, dn;
	if(gsl_sf_elljac_e(x, y, &sn, &cn, &dn) == GSL_SUCCESS)
		return dn;
	else
		return MATH_NAN;
}

/* Error Functions */
GSL_WRAP(lerfc,log_erfc);
GSL_WRAP(erfZ,erf_Z);
GSL_WRAP(erfQ,erf_Q);
GSL_WRAP(hazard,hazard);

/* Exponential Functions */
GSL_WRAP2(expmul,exp_mult);
GSL_WRAP(expm1,expm1);
GSL_WRAP(exprel,exprel);
GSL_WRAP(exprel2,exprel_2);
GSL_WRAP2_INT(expreln,exprel_n);

/* Exponential Integrals */
GSL_WRAP(E1,expint_E1);
GSL_WRAP(E2,expint_E2);
GSL_WRAP2_INT(En,expint_En);
GSL_WRAP(Ei,expint_Ei);
GSL_WRAP(Shi,Shi);
GSL_WRAP(Chi,Chi);
GSL_WRAP(Ei3,expint_3);
GSL_WRAP(Si,Si);
GSL_WRAP(Ci,Ci);
GSL_WRAP(atanint,atanint);

double
math_li(double x)
{
	return math_E1(log(x));
}

double
math_Li(double x)
{
	return math_li(x) - math_li(2.0);
}

/* Fermi-Dirac Function */
GSL_WRAP(fdm1,fermi_dirac_m1);
GSL_WRAP(fd0,fermi_dirac_0);
GSL_WRAP(fd1,fermi_dirac_1);
GSL_WRAP(fd2,fermi_dirac_2);
GSL_WRAP(fdmhalf,fermi_dirac_mhalf);
GSL_WRAP(fdhalf,fermi_dirac_half);
GSL_WRAP(fd3half,fermi_dirac_3half);
GSL_WRAP2(fdinc0,fermi_dirac_inc_0);

/* Gamma and Beta Functions */
GSL_WRAP(gammastar,gammastar);
GSL_WRAP(gammainv,gammainv);
GSL_WRAP2(poch,poch);
GSL_WRAP2(lpoch,lnpoch);
GSL_WRAP2(pochrel,pochrel);
GSL_WRAP2(gammainc,gamma_inc);
GSL_WRAP2(gammaincQ,gamma_inc_Q);
GSL_WRAP2(gammaincP,gamma_inc_P);
GSL_WRAP2(beta,beta);
GSL_WRAP2(lbeta,lnbeta);
GSL_WRAP3(betainc,beta_inc);

/* Gegenbauer Functions */
GSL_WRAP2(gegenb1,gegenpoly_1);
GSL_WRAP2(gegenb2,gegenpoly_2);
GSL_WRAP2(gegenb3,gegenpoly_3);
GSL_WRAP3_INT(gegenbn,gegenpoly_n);

/* Hypergeometric Functions */
GSL_WRAP2(hyperg0F1,hyperg_0F1);
GSL_WRAP3(hyperg1F1,hyperg_1F1);
GSL_WRAP3(hypergU,hyperg_U);
GSL_WRAP4(hyperg2F1,hyperg_2F1);
GSL_WRAP4(hyperg2F1c,hyperg_2F1_conj);
GSL_WRAP4(hyperg2F1r,hyperg_2F1_renorm);
GSL_WRAP4(hyperg2F1cr,hyperg_2F1_conj_renorm);
GSL_WRAP3(hyperg2F0,hyperg_2F0);

/* Laguerre Functions */
GSL_WRAP2(laguerre1,laguerre_1);
GSL_WRAP2(laguerre2,laguerre_2);
GSL_WRAP2(laguerre3,laguerre_3);
GSL_WRAP3_INT(laguerren,laguerre_n);

/* Lambert W Functions */
GSL_WRAP(W0,lambert_W0);

double
math_Wm1(double x)
{
	gsl_sf_result res;
	if(x < 0.0 && gsl_sf_lambert_Wm1_e(x, &res) == GSL_SUCCESS)
		return res.val;
	else
		return MATH_NAN;
}

/* Legendre Functions and Spherical Harmonics */
GSL_WRAP(legendreP1,legendre_P1);
GSL_WRAP(legendreP2,legendre_P2);
GSL_WRAP(legendreP3,legendre_P3);
GSL_WRAP2_INT(legendrePl,legendre_Pl);
GSL_WRAP(legendreQ0,legendre_Q0);
GSL_WRAP(legendreQ1,legendre_Q1);
GSL_WRAP2_INT(legendreQl,legendre_Ql);
GSL_WRAP3_INT2(legendrePlm,legendre_Plm);
GSL_WRAP3_INT2(sphPlm,legendre_sphPlm);
GSL_WRAP2(conicPhalf,conicalP_half);
GSL_WRAP2(conicPmhalf,conicalP_mhalf);
GSL_WRAP2(conicP0,conicalP_0);
GSL_WRAP2(conicP1,conicalP_1);
GSL_WRAP3_INT(conicPsph,conicalP_sph_reg);
GSL_WRAP3_INT(conicPcyl,conicalP_cyl_reg);
GSL_WRAP2(legendreH3d0,legendre_H3d_0);
GSL_WRAP2(legendreH3d1,legendre_H3d_1);
GSL_WRAP3_INT(legendreH3d,legendre_H3d);

/* Logarithm and Related Functions */
GSL_WRAP(labs,log_abs);
GSL_WRAP(log1px,log_1plusx);
GSL_WRAP(log1pxmx,log_1plusx_mx);

/* Mathieu Functions */
double
math2_mathieua(double x, double y)
{
	gsl_sf_result res;
	if(gsl_sf_mathieu_a_e((int)x, y, &res) == GSL_SUCCESS)
		return res.val;
	else
		return MATH_NAN;
}

double
math2_mathieub(double x, double y)
{
	gsl_sf_result res;
	if(gsl_sf_mathieu_b_e((int)x, y, &res) == GSL_SUCCESS)
		return res.val;
	else
		return MATH_NAN;
}

double
math3_mathieuce(double x, double y, double z)
{
	gsl_sf_result res;
	if(gsl_sf_mathieu_ce_e((int)x, y, z, &res) == GSL_SUCCESS)
		return res.val;
	else
		return MATH_NAN;
}

double
math3_mathieuse(double x, double y, double z)
{
	gsl_sf_result res;
	if(gsl_sf_mathieu_se_e((int)x, y, z, &res) == GSL_SUCCESS)
		return res.val;
	else
		return MATH_NAN;
}

double
math4_mathieuMc(double x, double y, double z, double w)
{
	gsl_sf_result res;
	if(gsl_sf_mathieu_Mc_e((int)x, (int)y, z, w, &res) == GSL_SUCCESS)
		return res.val;
	else
		return MATH_NAN;
}

double
math4_mathieuMs(double x, double y, double z, double w)
{
	gsl_sf_result res;
	if(gsl_sf_mathieu_Ms_e((int)x, (int)y, z, w, &res) == GSL_SUCCESS)
		return res.val;
	else
		return MATH_NAN;
}

/* Psi (Digamma) Function */
GSL_WRAP(digamma,psi);
GSL_WRAP(digamma1piy,psi_1piy);
GSL_WRAP(trigamma,psi_1);
GSL_WRAP2_INT(polygamma,psi_n);

/* Synchrotron Functions */
GSL_WRAP(synch1,synchrotron_1);
GSL_WRAP(synch2,synchrotron_2);

/* Transport Functions */
GSL_WRAP(transp2,transport_2);
GSL_WRAP(transp3,transport_3);
GSL_WRAP(transp4,transport_4);
GSL_WRAP(transp5,transport_5);

/* Trigonometric Functions */
GSL_WRAP(sinc,sinc);
GSL_WRAP(lsinh,lnsinh);
GSL_WRAP(lcosh,lncosh);

/* Zeta Functions */
GSL_WRAP(zeta,zeta);
GSL_WRAP(zetam1,zetam1);
GSL_WRAP2(hzeta,hzeta);
GSL_WRAP(eta,eta);

#endif
