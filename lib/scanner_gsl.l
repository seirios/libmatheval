/*
 * Copyright (C) 1999, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2011,
 * 2012, 2013 Free Software Foundation, Inc.
 * 
 * This file is part of GNU libmatheval
 *
 * GNU libmatheval is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * GNU libmatheval is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNU libmatheval.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

%{
/*
 * Copyright (C) 1999, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2011,
 * 2012, 2013 Free Software Foundation, Inc.
 * 
 * This file is part of GNU libmatheval
 *
 * GNU libmatheval is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * GNU libmatheval is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNU libmatheval.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#if HAVE_CONFIG_H
#  include "config.h"
#endif

#include <stdlib.h>
#include <string.h>
#include "node.h"
#include "parser.h"
#include "symbol_table.h"
%}

/* Do not include the default yywrap, as long as it is not needed */
%option bison-bridge reentrant
%option noyywrap nounput noinput
%option header-file="scanner_gsl.h"
%option extra-type="SymbolTable *"

/* Token definitions. */
whitespace [ \t]+
digit [0-9]
number ({digit}+|{digit}+\.{digit}*|{digit}*\.{digit}+)([Ee][+\-]?{digit}+)?
constant "e"|"log2e"|"log10e"|"ln2"|"ln10"|"pi"|"pi_2"|"pi_4"|"1_pi"|"2_pi"|"2_sqrtpi"|"sqrt2"|"sqrt1_2"|"NaN"|"C_c"|"C_mu0"|"C_eps0"|"C_h"|"C_hbar"|"C_faraday"|"C_kb"|"C_R0"|"C_V0"|"C_sigma"|"C_gauss"
function "acos"|"acosh"|"asin"|"asinh"|"atan"|"atanh"|"cbrt"|"ceil"|"cos"|"cosh"|"erf"|"erfc"|"exp"|"exp2"|"expm1"|"abs"|"floor"|"J0"|"J1"|"Jn"|"lgamma"|"log"|"log10"|"log1p"|"log2"|"round"|"sin"|"sinh"|"sqrt"|"tan"|"tanh"|"gamma"|"trunc"|"Y0"|"Y1"|"Yn"|"cot"|"sec"|"csc"|"acot"|"asec"|"acsc"|"coth"|"sech"|"csch"|"acoth"|"asech"|"acsch"|"step"|"delta"|"nandelta"|"sign"|"dist"|"sinc"|"E1"|"E2"|"En"|"Ei"|"Shi"|"Chi"|"Ei3"|"Si"|"Ci"|"atanint"|"li"|"Li"|"Ai"|"Bi"|"Ais"|"Bis"|"Aip"|"Bip"|"Aips"|"Bips"|"digamma"|"digamma1piy"|"trigamma"|"dilog"|"zeta"|"zetam1"|"hzeta"|"eta"|"W0"|"Wm1"|"clausen"|"dawson"|"debye1"|"debye2"|"debye3"|"debye4"|"debye5"|"debye6"|"fdm1"|"fd0"|"fd1"|"fd2"|"fdmhalf"|"fdhalf"|"fd3half"|"fdinc0"|"beta"|"lbeta"|"mod"|"gammainc"|"gammaincQ"|"gammaincP"|"gammastar"|"gammainv"|"poch"|"lpoch"|"pochrel"|"betainc"|"polygamma"|"lsinh"|"lcosh"|"lerfc"|"erfZ"|"erfQ"|"hazard"|"hyperg0F1"|"hyperg1F1"|"hypergU"|"hyperg2F1"|"hyperg2F1c"|"hyperg2F1r"|"hyperg2F1cr"|"hyperg2F0"|"labs"|"log1px"|"log1pxmx"|"expmul"|"expm1"|"exprel"|"exprel2"|"expreln"|"gegenb1"|"gegenb2"|"gegenb3"|"gegenbn"|"synch1"|"synch2"|"transp2"|"transp3"|"transp4"|"transp5"|"hydrogenR1"|"hydrogenR"|"coulombF"|"coulombG"|"coulombFp"|"coulombGp"|"coulombnorm"|"ellipsn"|"ellipcn"|"ellipdn"|"ellintKc"|"ellintEc"|"ellintPc"|"ellintF"|"ellintE"|"ellintP"|"ellintD"|"ellintRC"|"ellintRD"|"ellintRF"|"ellintRJ"|"laguerre1"|"laguerre2"|"laguerre3"|"laguerren"|"legendreP1"|"legendreP2"|"legendreP3"|"legendrePl"|"legendreQ0"|"legendreQ1"|"legendreQl"|"legendrePlm"|"sphPlm"|"conicPhalf"|"conicPmhalf"|"conicP0"|"conicP1"|"conicPsph"|"conicPcyl"|"legendreH3d0"|"legendreH3d1"|"legendreH3d"|"mathieua"|"mathieub"|"mathieuce"|"mathieuse"|"mathieuMc"|"mathieuMs"|"besselI0"|"besselI1"|"besselIn"|"besselI0s"|"besselI1s"|"besselIns"|"besselK0"|"besselK1"|"besselKn"|"besselK0s"|"besselK1s"|"besselKns"|"besselj0"|"besselj1"|"besselj2"|"besseljl"|"bessely0"|"bessely1"|"bessely2"|"besselyl"|"besselJnu"|"besselYnu"|"besselInu"|"besselKnu"
identifier [_a-zA-Z][[_a-zA-Z0-9]*

%%

{whitespace}

{number} {
        /* Create node representing constant with appropriate value. */
        yylval->node = node_create ('n', atof (yytext));
        return NUMBER;
}

{constant} {
        Record *record; /* Symbol table record. */

        /* Find symbol table record corresponding to constant name. */
        record = symbol_table_lookup (yyextra, yytext);
        yylval->node = node_create ('c', record);
        return CONSTANT;
}

{function} {
        /* Find symbol table record corresponding to function name. */
        yylval->record = symbol_table_lookup (yyextra, yytext);
        return FUNCTION;
}

{identifier} {
        Record *record; /* Symbol table record. */

        /* Insert variable into symbol table. */
        record = symbol_table_insert (yyextra, yytext, 'v');
        yylval->node = node_create ('v', record);
        return VARIABLE;
}

"," {
		return ',';
}

"+" {
        return '+';
}

"-" {
        return '-';
}

"*" {
        return '*';
}

"/" {
        return '/';
}

"^" {
        return '^';
}

"(" {
        return '(';
}

")" {
        return ')';
}

"\n" {
        return '\n';
}

%%
