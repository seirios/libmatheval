/* 
 * Copyright (C) 1999, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2011,
 * 2012, 2013 Free Software Foundation, Inc.
 * 
 * This file is part of GNU libmatheval
 * 
 * GNU libmatheval is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * GNU libmatheval is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNU libmatheval.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef XMATH_H
#define XMATH_H 1

#if HAVE_CONFIG_H
#include "config.h"
#endif

#if HAVE_MATH_H
#include <math.h>
#else
#error no <math.h> avaialable
#endif

#ifdef HAVE_GSL_GSL_MATH_H
#include <gsl/gsl_math.h>
#define USE_GSL_MATH 1
#endif

#ifdef HAVE_GSL_GSL_SF_H
#include <gsl/gsl_sf.h>
#define USE_GSL 1
#endif

#ifdef HAVE_GSL_GSL_CONST_MKSA_H
#include <gsl/gsl_const_mksa.h>
#define USE_GSL_CONST 1
#endif

#ifdef INFINITY
#define MATH_INFINITY INFINITY
#elif defined HAVE_GSL_GSL_MATH_H
#define MATH_INFINITY GSL_POSINF
#elif defined HUGE_VAL
#define MATH_INFINITY HUGE_VAL
#else
#define MATH_INFINITY (1.0/0.0)
#endif

#ifdef NAN
#define MATH_NAN NAN
#elif defined HAVE_GSL_GSL_MATH_H
#define MATH_NAN GSL_NAN
#elif defined INFINITY
#define MATH_NAN (INFINITY/INFINITY)
#else
#define MATH_NAN (0.0/0.0)
#endif

#ifdef isnan
#define MATH_ISNAN(x) isnan(x)
#elif defined HAVE_GSL_GSL_MATH_H
#define MATH_ISNAN(x) gsl_isnan(x)
#else
#define MATH_ISNAN(x) ((x) != (x))
#endif

#ifdef isinf
#define MATH_ISINF(x) isinf(x)
#elif defined HAVE_GSL_GSL_MATH_H
#define MATH_ISINF(x) gsl_isinf(x)
#endif

/* Calculate cotangent of value x.  */
double          math_cot(double x);

/* Calculate secant of value x.  */
double          math_sec(double x);

/* Calculate cosecant of value x.  */
double          math_csc(double x);

/* Calculate inverse cotangent of value x.  */
double          math_acot(double x);

/* Calculate inverse secant of value x.  */
double          math_asec(double x);

/* Calculate inverse cosecant of value x.  */
double          math_acsc(double x);

/* Calculate hyperbolical cotangent of value x.  */
double          math_coth(double x);

/* Calculate hyperbolical secant of value x.  */
double          math_sech(double x);

/* Calculate hyperbolical cosecant of value x.  */
double          math_csch(double x);

/* Calculate inverse hyperbolical cotangent of value x.  */
double          math_acoth(double x);

/* Calculate inverse hyperbolical secant of value x.  */
double          math_asech(double x);

/* Calculate inverse hyperbolical cosecant of value x.  */
double          math_acsch(double x);

/* Calculate Heaviside step function value for given value x.  */
double          math_step(double x);

/* Calculate Dirac delta function value for given value x.  */
double          math_delta(double x);

/* Calculate variation of Dirac delta function (with not-a-number instead
 * of infinity value for x= 0) value for given value x. */
double          math_nandelta(double x);

/* Calculate sign function value for given value x. */
double			math_sign(double x);

/* Calculate Bessel function of the first kind of order n for given value x. */
double			math_jn(double n,double x);

/* Calculate Bessel function of the second kind of order n for given value x. */
double			math_yn(double n,double x);

#ifdef USE_GSL
/* Airy Functions and Derivatives */
double			math_Ai(double x);
double			math_Bi(double x);
double			math_Ais(double x);
double			math_Bis(double x);
double			math_Aip(double x);
double			math_Bip(double x);
double			math_Aips(double x);
double			math_Bips(double x);

/* Bessel Functions */
double			math_besselI0(double x);
double			math_besselI1(double x);
double			math2_besselIn(double x,double y);
double			math_besselI0s(double x);
double			math_besselI1s(double x);
double			math2_besselIns(double x,double y);
double			math_besselK0(double x);
double			math_besselK1(double x);
double			math2_besselKn(double x,double y);
double			math_besselK0s(double x);
double			math_besselK1s(double x);
double			math2_besselKns(double x,double y);
double			math_besselj0(double x);
double			math_besselj1(double x);
double			math_besselj2(double x);
double			math2_besseljl(double x,double y);
double			math_bessely0(double x);
double			math_bessely1(double x);
double			math_bessely2(double x);
double			math2_besselyl(double x,double y);
double			math2_besselJnu(double x,double y);
double			math2_besselYnu(double x,double y);
double			math2_besselInu(double x,double y);
double			math2_besselKnu(double x,double y);

/* Clausen Functions */
double			math_clausen(double x);

/* Coulomb Functions */
double			math2_hydrogenR1(double x,double y);
double			math4_hydrogenR(double x,double y,double z,double w);
double			math4_coulombF(double x,double y,double z,double w);
double			math4_coulombG(double x,double y,double z,double w);
double			math4_coulombFp(double x,double y,double z,double w);
double			math4_coulombGp(double x,double y,double z,double w);
double			math2_coulombnorm(double x,double y);

/* Dawson Function */
double			math_dawson(double x);

/* Debye Functions */
double			math_debye1(double x);
double			math_debye2(double x);
double			math_debye3(double x);
double			math_debye4(double x);
double			math_debye5(double x);
double			math_debye6(double x);

/* Dilogarithm */
double			math_dilog(double x);

/* Elliptic Integrals */
double			math_ellintKc(double x);
double			math_ellintEc(double x);
double			math2_ellintPc(double x,double y);
double			math2_ellintF(double x,double y);
double			math2_ellintE(double x,double y);
double			math3_ellintP(double x,double y,double z);
double			math2_ellintD(double x,double y);
double			math2_ellintRC(double x,double y);
double			math3_ellintRD(double x,double y,double z);
double			math3_ellintRF(double x,double y,double z);
double			math4_ellintRJ(double x,double y,double z,double w);

/* Elliptic Functions (Jacobi) */
double			math2_ellipsn(double x,double y);
double			math2_ellipcn(double x,double y);
double			math2_ellipdn(double x,double y);

/* Error Functions */
double			math_lerfc(double x);
double			math_erfZ(double x);
double			math_erfQ(double x);
double			math_hazard(double x);

/* Exponential Functions */
double			math2_expmul(double x,double y);
double			math_expm1(double x);
double			math_exprel(double x);
double			math_exprel2(double x);
double			math2_expreln(double x,double y);

/* Exponential Integrals */
double			math_E1(double x);
double			math_E2(double x);
double			math2_En(double x,double y);
double			math_Ei(double x);
double			math_Shi(double x);
double			math_Chi(double x);
double			math_Ei3(double x);
double			math_Si(double x);
double			math_Ci(double x);
double			math_atanint(double x);
double			math_li(double x);
double			math_Li(double x);

/* Fermi-Dirac Function */
double			math_fdm1(double x);
double			math_fd0(double x);
double			math_fd1(double x);
double			math_fd2(double x);
double			math_fdmhalf(double x);
double			math_fdhalf(double x);
double			math_fd3half(double x);
double			math2_fdinc0(double x,double y);

/* Gamma and Beta Functions */
double			math_gammastar(double x);
double			math_gammainv(double x);
double			math2_poch(double x,double y);
double			math2_lpoch(double x,double y);
double			math2_pochrel(double x,double y);
double			math2_gammainc(double x, double y);
double			math2_gammaincQ(double x, double y);
double			math2_gammaincP(double x, double y);
double			math2_beta(double x, double y);
double			math2_lbeta(double x, double y);
double			math3_betainc(double x,double y,double z);

/* Gegenbauer Functions */
double			math2_gegenb1(double x,double y);
double			math2_gegenb2(double x,double y);
double			math2_gegenb3(double x,double y);
double			math3_gegenbn(double x,double y,double z);

/* Hypergeometric Functions */
double			math2_hyperg0F1(double x,double y);
double			math3_hyperg1F1(double x,double y,double z);
double			math3_hypergU(double x,double y,double z);
double			math4_hyperg2F1(double x,double y,double z,double w);
double			math4_hyperg2F1c(double x,double y,double z,double w);
double			math4_hyperg2F1r(double x,double y,double z,double w);
double			math4_hyperg2F1cr(double x,double y,double z,double w);
double			math3_hyperg2F0(double x,double y,double z);

/* Laguerre Functions */
double			math2_laguerre1(double x,double y);
double			math2_laguerre2(double x,double y);
double			math2_laguerre3(double x,double y);
double			math3_laguerren(double x,double y,double z);

/* Lambert W Functions */
double			math_W0(double x);
double			math_Wm1(double x);

/* Legendre Functions and Spherical Harmonics */
double			math_legendreP1(double x);
double			math_legendreP2(double x);
double			math_legendreP3(double x);
double			math2_legendrePl(double x,double y);
double			math_legendreQ0(double x);
double			math_legendreQ1(double x);
double			math2_legendreQl(double x,double y);
double			math3_legendrePlm(double x,double y,double z);
double			math3_sphPlm(double x,double y,double z);
double			math2_conicPhalf(double x,double y);
double			math2_conicPmhalf(double x,double y);
double			math2_conicP0(double x,double y);
double			math2_conicP1(double x,double y);
double			math3_conicPsph(double x,double y,double z);
double			math3_conicPcyl(double x,double y,double z);
double			math2_legendreH3d0(double x,double y);
double			math2_legendreH3d1(double x,double y);
double			math3_legendreH3d(double x,double y,double z);

/* Logarithm and Related Functions */
double			math_labs(double x);
double			math_log1px(double x);
double			math_log1pxmx(double x);

/* Mathieu Functions */
double			math2_mathieua(double x,double y);
double			math2_mathieub(double x,double y);
double			math3_mathieuce(double x,double y,double z);
double			math3_mathieuse(double x,double y,double z);
double			math4_mathieuMc(double x,double y,double z,double w);
double			math4_mathieuMs(double x,double y,double z,double w);

/* Psi (Digamma) Function */
double			math_digamma(double x);
double			math_digamma1piy(double x);
double			math_trigamma(double x);
double			math2_polygamma(double x,double y);

/* Synchrotron Functions */
double			math_synch1(double x);
double			math_synch2(double x);

/* Transport Functions */
double			math_transp2(double x);
double			math_transp3(double x);
double			math_transp4(double x);
double			math_transp5(double x);

/* Trigonometric Functions */
double			math_sinc(double x);
double			math_lsinh(double x);
double			math_lcosh(double x);

/* Zeta Functions */
double			math_zeta(double x);
double			math_zetam1(double x);
double			math2_hzeta(double x,double y);
double			math_eta(double x);
#endif
#endif
