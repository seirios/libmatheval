/* 
 * Copyright (C) 1999, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2011,
 * 2012, 2013 Free Software Foundation, Inc.
 * 
 * This file is part of GNU libmatheval
 * 
 * GNU libmatheval is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * GNU libmatheval is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNU libmatheval.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#if HAVE_CONFIG_H
#include "config.h"
#endif

#include <math.h>
#include <assert.h>
#include <stdarg.h>
#include "common.h"
#include "node.h"

#ifdef NAN
#define MATH_NAN NAN
#elif defined INFINITY
#define MATH_NAN (INFINITY/INFINITY)
#else
#define MATH_NAN (0.0/0.0)
#endif

Node           *
node_create(char type, ...)
{
	Node           *node;	/* New node.  */
	va_list         ap;	/* Variable argument list.  */

	/* Allocate memory for node and initialize its type. */
	node = XMALLOC(Node, 1);
	node->type = type;

	/* According to node type, initialize rest of the node from
	 * variable argument list. */
	va_start(ap, type);
	switch (node->type) {
	case 'n':
		/* Initialize number value. */
		node->data.number = va_arg(ap, double);
		break;

	case 'c':
		/* Remember pointer to symbol table record describing
		 * constant. */
		node->data.constant = va_arg(ap, Record *);
		break;

	case 'v':
		/* Remember pointer to symbol table record describing
		 * variable. */
		node->data.variable = va_arg(ap, Record *);
		break;

	case 'f':
		/* Remember pointer to symbol table record describing
		 * function and initialize function argument. */
		node->data.function.record = va_arg(ap, Record *);
		node->data.function.child = va_arg(ap, Node *);
		break;

	case 'g':
		/* As above but for function of two arguments */
		node->data.function2.record = va_arg(ap, Record *);
		node->data.function2.first = va_arg(ap, Node *);
		node->data.function2.second = va_arg(ap, Node *);
		break;

	case 'h':
		/* As above but for function of three arguments */
		node->data.function3.record = va_arg(ap, Record *);
		node->data.function3.first = va_arg(ap, Node *);
		node->data.function3.second = va_arg(ap, Node *);
		node->data.function3.third = va_arg(ap, Node *);
		break;

	case 'q':
		/* As above but for function of four arguments */
		node->data.function4.record = va_arg(ap, Record *);
		node->data.function4.first = va_arg(ap, Node *);
		node->data.function4.second = va_arg(ap, Node *);
		node->data.function4.third = va_arg(ap, Node *);
		node->data.function4.fourth = va_arg(ap, Node *);
		break;

	case 'u':
		/* Initialize operation type and operand. */
		node->data.un_op.operation = (char) va_arg(ap, int);

		node->data.un_op.child = va_arg(ap, Node *);
		break;

	case 'b':
		/* Initialize operation type and operands. */
		node->data.un_op.operation = (char) va_arg(ap, int);

		node->data.bin_op.left = va_arg(ap, Node *);
		node->data.bin_op.right = va_arg(ap, Node *);
		break;

	default:
		assert(0);
	}
	va_end(ap);

	return node;
}

void
node_destroy(Node * node)
{
	/* Skip if node already null (this may occur during
	 * simplification). */
	if (!node)
		return;

	/* If necessary, destroy subtree rooted at node. */
	switch (node->type) {
	case 'n':
	case 'c':
	case 'v':
		break;

	case 'f':
		node_destroy(node->data.function.child);
		break;

	case 'g':
		node_destroy(node->data.function2.first);
		node_destroy(node->data.function2.second);
		break;

	case 'h':
		node_destroy(node->data.function3.first);
		node_destroy(node->data.function3.second);
		node_destroy(node->data.function3.third);
		break;

	case 'q':
		node_destroy(node->data.function4.first);
		node_destroy(node->data.function4.second);
		node_destroy(node->data.function4.third);
		node_destroy(node->data.function4.fourth);
		break;

	case 'u':
		node_destroy(node->data.un_op.child);
		break;

	case 'b':
		node_destroy(node->data.bin_op.left);
		node_destroy(node->data.bin_op.right);
		break;
	}

	/* Deallocate memory used by node. */
	XFREE(node);
}

Node           *
node_copy(Node * node)
{
	/* According to node type, create (deep) copy of subtree rooted at 
	 * node. */
	switch (node->type) {
	case 'n':
		return node_create('n', node->data.number);

	case 'c':
		return node_create('c', node->data.constant);

	case 'v':
		return node_create('v', node->data.variable);

	case 'f':
		return node_create('f', node->data.function.record,
				   node_copy(node->data.function.child));
	
	case 'g':
		return node_create('g', node->data.function2.record,
				   node_copy(node->data.function2.first),
				   node_copy(node->data.function2.second));

	case 'h':
		return node_create('h', node->data.function3.record,
				   node_copy(node->data.function3.first),
				   node_copy(node->data.function3.second),
				   node_copy(node->data.function3.third));

	case 'q':
		return node_create('q', node->data.function4.record,
				   node_copy(node->data.function4.first),
				   node_copy(node->data.function4.second),
				   node_copy(node->data.function4.third),
				   node_copy(node->data.function4.fourth));

	case 'u':
		return node_create('u', node->data.un_op.operation,
				   node_copy(node->data.un_op.child));

	case 'b':
		return node_create('b', node->data.bin_op.operation,
				   node_copy(node->data.bin_op.left),
				   node_copy(node->data.bin_op.right));
	}

    return NULL;
}

Node           *
node_simplify(Node * node)
{
	/* According to node type, apply further simplifications.
	 * Constants are not simplified, in order to eventually appear
	 * unchanged in derivatives. */
	switch (node->type) {
	case 'n':
	case 'c':
	case 'v':
		return node;

	case 'f':
		/* Simplify function argument and if number evaluate
		 * function and replace function node with number node. */
		node->data.function.child =
		    node_simplify(node->data.function.child);
		if (node->data.function.child->type == 'n') {
			double          value = node_evaluate(node);

			node_destroy(node);
			return node_create('n', value);
		} else
			return node;

	case 'g':
		/* Simplify function arguments and if numbers evaluate
		 * function and replace function node with number node. */
		node->data.function2.first =
			node_simplify(node->data.function2.first);
		node->data.function2.second =
			node_simplify(node->data.function2.second);
		if (node->data.function2.first->type == 'n' &&
				node->data.function2.second->type == 'n') {
			double			value = node_evaluate(node);

			node_destroy(node);
			return node_create('n', value);
		} else
			return node;

	case 'h':
		/* Simplify function arguments and if numbers evaluate
		 * function and replace function node with number node. */
		node->data.function3.first =
			node_simplify(node->data.function3.first);
		node->data.function3.second =
			node_simplify(node->data.function3.second);
		node->data.function3.third =
			node_simplify(node->data.function3.third);
		if (node->data.function3.first->type == 'n' &&
				node->data.function3.second->type == 'n' &&
				node->data.function3.third->type == 'n') {
			double			value = node_evaluate(node);

			node_destroy(node);
			return node_create('n', value);
		} else
			return node;

	case 'q':
		/* Simplify function arguments and if numbers evaluate
		 * function and replace function node with number node. */
		node->data.function4.first =
			node_simplify(node->data.function4.first);
		node->data.function4.second =
			node_simplify(node->data.function4.second);
		if (node->data.function4.first->type == 'n' &&
				node->data.function4.second->type == 'n' &&
				node->data.function4.third->type == 'n' &&
				node->data.function4.fourth->type == 'n') {
			double			value = node_evaluate(node);

			node_destroy(node);
			return node_create('n', value);
		} else
			return node;

	case 'u':
		/* Simplify unary operation operand and if number apply
		 * operation and replace operation node with number node. */
		node->data.un_op.child =
		    node_simplify(node->data.un_op.child);
		if (node->data.un_op.operation == '-'
		    && node->data.un_op.child->type == 'n') {
			double          value = node_evaluate(node);

			node_destroy(node);
			return node_create('n', value);
		} else
			return node;

	case 'b':
		/* Simplify binary operation operands. */
		node->data.bin_op.left =
		    node_simplify(node->data.bin_op.left);
		node->data.bin_op.right =
		    node_simplify(node->data.bin_op.right);

		/* If operands numbers apply operation and replace
		 * operation node with number node. */
		if (node->data.bin_op.left->type == 'n'
		    && node->data.bin_op.right->type == 'n') {
			double          value = node_evaluate(node);

			node_destroy(node);
			return node_create('n', value);
		}
		/* Eliminate 0 as neutral addition operand. */
		else if (node->data.bin_op.operation == '+')
			if (node->data.bin_op.left->type == 'n'
			    && node->data.bin_op.left->data.number == 0) {
				Node           *right;

				right = node->data.bin_op.right;
				node->data.bin_op.right = NULL;
				node_destroy(node);
				return right;
			} else if (node->data.bin_op.right->type == 'n'
				   && node->data.bin_op.right->data.
				   number == 0) {
				Node           *left;

				left = node->data.bin_op.left;
				node->data.bin_op.left = NULL;
				node_destroy(node);
				return left;
			} else
				return node;
		/* Eliminate 0 as neutral subtraction right operand. */
		else if (node->data.bin_op.operation == '-')
			if (node->data.bin_op.right->type == 'n'
			    && node->data.bin_op.right->data.number == 0) {
				Node           *left;

				left = node->data.bin_op.left;
				node->data.bin_op.left = NULL;
				node_destroy(node);
				return left;
			} else
				return node;
		/* Eliminate 1 as neutral multiplication operand. */
		else if (node->data.bin_op.operation == '*')
			if (node->data.bin_op.left->type == 'n'
			    && node->data.bin_op.left->data.number == 1) {
				Node           *right;

				right = node->data.bin_op.right;
				node->data.bin_op.right = NULL;
				node_destroy(node);
				return right;
			} else if (node->data.bin_op.right->type == 'n'
				   && node->data.bin_op.right->data.
				   number == 1) {
				Node           *left;

				left = node->data.bin_op.left;
				node->data.bin_op.left = NULL;
				node_destroy(node);
				return left;
			} else
				return node;
		/* Eliminate 1 as neutral division right operand. */
		else if (node->data.bin_op.operation == '/')
			if (node->data.bin_op.right->type == 'n'
			    && node->data.bin_op.right->data.number == 1) {
				Node           *left;

				left = node->data.bin_op.left;
				node->data.bin_op.left = NULL;
				node_destroy(node);
				return left;
			} else
				return node;
		/* Eliminate 0 and 1 as both left and right exponentiation 
		 * operands. */
		else if (node->data.bin_op.operation == '^')
			if (node->data.bin_op.left->type == 'n'
			    && node->data.bin_op.left->data.number == 0) {
				node_destroy(node);
				return node_create('n', 0.0);
			} else if (node->data.bin_op.left->type == 'n'
				   && node->data.bin_op.left->data.
				   number == 1) {
				node_destroy(node);
				return node_create('n', 1.0);
			} else if (node->data.bin_op.right->type == 'n'
				   && node->data.bin_op.right->data.
				   number == 0) {
				node_destroy(node);
				return node_create('n', 1.0);
			} else if (node->data.bin_op.right->type == 'n'
				   && node->data.bin_op.right->data.
				   number == 1) {
				Node           *left;

				left = node->data.bin_op.left;
				node->data.bin_op.left = NULL;
				node_destroy(node);
				return left;
			} else
				return node;
		else
			return node;
	}

    return node;
}

double
node_evaluate(Node * node)
{
	/* According to node type, evaluate subtree rooted at node. */
	switch (node->type) {
	case 'n':
		return node->data.number;

	case 'c':
		/* Constant values are used from symbol table. */
		return node->data.constant->data.value;

	case 'v':
		/* Variable values are used from symbol table. */
		return node->data.variable->data.value;

	case 'f':
		/* Functions are evaluated through symbol table. */
		return (*node->data.function.record->data.
			function) (node_evaluate(node->data.function.child));
	
	case 'g':
		/* Functions are evaluated through symbol table. */
		if(node->data.function2.record->type == 'g')
			return (*node->data.function2.record->data.
					function2) (node_evaluate(node->data.function2.first),
						node_evaluate(node->data.function2.second));
		break;

	case 'h':
		/* Functions are evaluated through symbol table. */
		if(node->data.function3.record->type == 'h')
			return (*node->data.function3.record->data.
					function3) (node_evaluate(node->data.function3.first),
						node_evaluate(node->data.function3.second),
						node_evaluate(node->data.function3.third));
		break;

	case 'q':
		/* Functions are evaluated through symbol table. */
		if(node->data.function4.record->type == 'q')
			return (*node->data.function4.record->data.
					function4) (node_evaluate(node->data.function4.first),
						node_evaluate(node->data.function4.second),
						node_evaluate(node->data.function4.third),
						node_evaluate(node->data.function4.fourth));
		break;

	case 'u':
		/* Unary operation node is evaluated according to
		 * operation type. */
		switch (node->data.un_op.operation) {
		case '-':
			return -node_evaluate(node->data.un_op.child);
		}
        break;

	case 'b':
		/* Binary operation node is evaluated according to
		 * operation type. */
		switch (node->data.un_op.operation) {
		case '+':
			return node_evaluate(node->data.bin_op.left) +
			    node_evaluate(node->data.bin_op.right);

		case '-':
			return node_evaluate(node->data.bin_op.left) -
			    node_evaluate(node->data.bin_op.right);

		case '*':
			return node_evaluate(node->data.bin_op.left) *
			    node_evaluate(node->data.bin_op.right);

		case '/':
			return node_evaluate(node->data.bin_op.left) /
			    node_evaluate(node->data.bin_op.right);

		case '^':
			return pow(node_evaluate(node->data.bin_op.left),
				   node_evaluate(node->data.bin_op.right));
		}
        break;
	}

	return MATH_NAN;
}

void
node_flag_variables(Node * node)
{
	/* According to node type, flag variable in symbol table or
	 * proceed with calling function recursively on node children. */
	switch (node->type) {
	case 'v':
		node->data.variable->flag = TRUE;
		break;

	case 'f':
		node_flag_variables(node->data.function.child);
		break;

	case 'g':
		node_flag_variables(node->data.function2.first);
		node_flag_variables(node->data.function2.second);
		break;

	case 'h':
		node_flag_variables(node->data.function3.first);
		node_flag_variables(node->data.function3.second);
		node_flag_variables(node->data.function3.third);
		break;

	case 'q':
		node_flag_variables(node->data.function4.first);
		node_flag_variables(node->data.function4.second);
		node_flag_variables(node->data.function4.third);
		node_flag_variables(node->data.function4.fourth);
		break;

	case 'u':
		node_flag_variables(node->data.un_op.child);
		break;

	case 'b':
		node_flag_variables(node->data.bin_op.left);
		node_flag_variables(node->data.bin_op.right);
		break;
	}
}

int
node_get_length(Node * node)
{
	FILE           *file;	/* Temporary file. */
	int             count;	/* Count of bytes written to above file. */
	int             length;	/* Length of above string. */

	/* According to node type, calculate length of string representing 
	 * subtree rooted at node. */
	switch (node->type) {
	case 'n':
		length = 0;
		if (node->data.number < 0)
			length += 1;

		file = tmpfile();
		if (file) {
			if ((count =
			     fprintf(file, "%g", node->data.number)) >= 0)
				length += count;
			fclose(file);
		}

		if (node->data.number < 0)
			length += 1;
		return length;

	case 'c':
		return strlen(node->data.constant->name);

	case 'v':
		return strlen(node->data.variable->name);

	case 'f':
		return strlen(node->data.function.record->name) + 1 +
		    node_get_length(node->data.function.child) + 1;
		break;

	case 'g':
		return strlen(node->data.function2.record->name) + 1 +
			node_get_length(node->data.function2.first) + 1 +
			node_get_length(node->data.function2.second) + 1;
		break;

	case 'h':
		return strlen(node->data.function3.record->name) + 1 +
			node_get_length(node->data.function3.first) + 1 +
			node_get_length(node->data.function3.second) + 1 +
			node_get_length(node->data.function3.third) + 1;
		break;

	case 'q':
		return strlen(node->data.function4.record->name) + 1 +
			node_get_length(node->data.function4.first) + 1 +
			node_get_length(node->data.function4.second) + 1 +
			node_get_length(node->data.function4.third) + 1 +
			node_get_length(node->data.function4.fourth) + 1;
		break;

	case 'u':
		return 1 + 1 + node_get_length(node->data.un_op.child) + 1;

	case 'b':
		return 1 + node_get_length(node->data.bin_op.left) + 1 +
		    node_get_length(node->data.bin_op.right) + 1;
	}

	return 0;
}

void
node_write(Node * node, char *string)
{
	/* According to node type, write subtree rooted at node to node
	 * string variable.  Always use parenthesis to resolve operation
	 * precedence. */
	switch (node->type) {
	case 'n':
		if (node->data.number < 0) {
			sprintf(string, "%c", '(');
			string += strlen(string);
		}
		sprintf(string, "%g", node->data.number);
		string += strlen(string);
		if (node->data.number < 0)
			sprintf(string, "%c", ')');
		break;

	case 'c':
		sprintf(string, "%s", node->data.constant->name);
		break;

	case 'v':
		sprintf(string, "%s", node->data.variable->name);
		break;

	case 'f':
		sprintf(string, "%s%c", node->data.function.record->name,
			'(');
		string += strlen(string);
		node_write(node->data.function.child, string);
		string += strlen(string);
		sprintf(string, "%c", ')');
		break;
	
	case 'g':
		sprintf(string, "%s%c", node->data.function2.record->name,
			'(');
		string += strlen(string);
		node_write(node->data.function2.first, string);
		string += strlen(string);
		sprintf(string, "%c", ',');
		string += strlen(string);
		node_write(node->data.function2.second, string);
		string += strlen(string);
		sprintf(string, "%c", ')');
		break;

	case 'h':
		sprintf(string, "%s%c", node->data.function3.record->name,
			'(');
		string += strlen(string);
		node_write(node->data.function3.first, string);
		string += strlen(string);
		sprintf(string, "%c", ',');
		string += strlen(string);
		node_write(node->data.function3.second, string);
		string += strlen(string);
		sprintf(string, "%c", ',');
		string += strlen(string);
		node_write(node->data.function3.third, string);
		string += strlen(string);
		sprintf(string, "%c", ')');
		break;

	case 'q':
		sprintf(string, "%s%c", node->data.function4.record->name,
			'(');
		string += strlen(string);
		node_write(node->data.function4.first, string);
		string += strlen(string);
		sprintf(string, "%c", ',');
		string += strlen(string);
		node_write(node->data.function4.second, string);
		string += strlen(string);
		sprintf(string, "%c", ',');
		string += strlen(string);
		node_write(node->data.function4.third, string);
		string += strlen(string);
		sprintf(string, "%c", ',');
		string += strlen(string);
		node_write(node->data.function4.fourth, string);
		string += strlen(string);
		sprintf(string, "%c", ')');
		break;

	case 'u':
		sprintf(string, "%c", '(');
		string += strlen(string);
		sprintf(string, "%c", node->data.un_op.operation);
		string += strlen(string);
		node_write(node->data.un_op.child, string);
		string += strlen(string);
		sprintf(string, "%c", ')');
		break;

	case 'b':
		sprintf(string, "%c", '(');
		string += strlen(string);
		node_write(node->data.bin_op.left, string);
		string += strlen(string);
		sprintf(string, "%c", node->data.bin_op.operation);
		string += strlen(string);
		node_write(node->data.bin_op.right, string);
		string += strlen(string);
		sprintf(string, "%c", ')');
		break;
	}
}
