# libmatheval
A fork of GNU libmatheval supporting evaluation of many mathematical functions.

GNU libmatheval is a library (callable from C and Fortran) to parse and evaluate symbolic
expressions input as text. It supports expressions in any number of variables of arbitrary
names, decimal and symbolic constants, basic unary and binary operators, and elementary
mathematical functions. In addition to parsing and evaluation, libmatheval can also
compute symbolic derivatives and output expressions to strings.

## Goal
My goal is to add support for more functions to the evaluator part of libmatheval so they
can be plotted in xmplot. The idea is to add support for all of GSL's special functions,
using standard notations (eg. Ai(x) for Airy).

Seems like support for polynomials would be nice, so I'll include use of POLPAK.

I don't care about the symbolic differentiation part, so I won't be implementing that for
the functions added (couldn't anyway...).

## libm Functions

+ acos, acosh, asin, asinh, atan, atanh, cbrt, ceil, cos, cosh, erf, erfc, exp, exp2, expm1, fabs, floor, fmod, j0, j1, lgamma, log, log10, log1p, log2, round, sin, sinh, sqrt, tan, tanh, tgamma, trunc, y0, y1

## Additional Functions

+ cot, sec, csc, acot, asec, acsc, coth, sech, csch, acoth, asech, acsch, step, delta, nandelta, sign

## GSL Function Coverage

We consider here only real functions of one or more real arguments, and when not already included in libm

+ Airy Functions and Derivatives [DONE]
+ Bessel Functions [DONE]
+ Clausen Functions [DONE]
+ Coulomb Functions [DONE]
+ Coupling Coefficients [NO]
+ Dawson Function [DONE]
+ Debye Functions [DONE]
+ Dilogarithm [DONE]
+ Elementary Operations [NO]
+ Elliptic Integrals [DONE]
+ Elliptic Functions (Jacobi) [DONE]
+ Error Functions [DONE]
+ Exponential Functions [DONE]
+ Exponential Integrals [DONE]
+ Fermi-Dirac Function [DONE]
+ Gamma and Beta Functions [DONE]
+ Gegenbauer Functions [DONE]
+ Hypergeometric Functions [DONE]
+ Laguerre Functions [DONE]
+ Lambert W Functions [DONE]
+ Legendre Functions and Spherical Harmonics [DONE]
+ Logarithm and Related Functions [DONE]
+ Mathieu Functions [DONE]
+ Power Function [NO]
+ Psi (Digamma) Function [DONE]
+ Synchrotron Functions [DONE]
+ Transport Functions [DONE]
+ Trigonometric Functions [DONE]
+ Zeta Functions [DONE]

## GSL Constants
+ Fundamental constants (in MKSA system)

## POLPAK Polynomials Coverage

+ Bernoulli
+ Bernstein
+ Cardan
+ Charlier
+ Chebyshev
+ Euler
+ Gegenbauer [GSL]
+ Hermite
+ Jacobi
+ Krawtchouk
+ Laguerre [GSL]
+ Legendre [GSL]
+ Meixner
+ Zernike

## Other modifications
+ Removed yywrap dependency (thanks DarkBatcher)
+ No more autoconf warnings
+ Fixed misplaced {}'s in texinfo manual (gcc complained...)
+ No compilation warnings!
+ Added support for functions of two, three and four arguments
+ Dropped Fortran interface
+ Dropped support for symbolic derivatives
+ Fixed memleak due to discarded symbols on parser error
+ Removed testsuite and guile dependencies
+ Made thread-safe by using reentrant lexer/parser
